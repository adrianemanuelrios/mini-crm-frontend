import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { Observable, ReplaySubject } from 'rxjs';
@Component({
  selector: 'fury-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  constructor(private router: Router) {
  }
  private static isInitialLoad = true;
  private _gap = 16;
  isLoading: boolean;
  gap = `${this._gap}px`;

  title = 'Servers'
  subtitle = 'your servers'
  //SERVERS
  servers = [
    {id:1,name:'Server 1', status:'ONLINE'},
    {id:2,name:'Server 2', status:'OFFLINE'},
  ]

  see(item) {
    this.router.navigate(['/apps/server', item.id]);
  }
  edit(item) {

  }
  delete(item) {

  }

  reload() {
    this.isLoading = true;

    setTimeout(() => {
      this.isLoading = false;
    }, 2000);
  }
  col(colAmount: number) {
    return `1 1 calc(${100 / colAmount}% - ${this._gap - (this._gap / colAmount)}px)`;
  }
  ngOnInit() {
  }
}
