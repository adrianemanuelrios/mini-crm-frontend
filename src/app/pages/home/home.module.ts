import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../../../@fury/shared/material-components.module';
import { FuryCardModule } from '../../../@fury/shared/card/card.module';
import { LoadingOverlayModule } from '../../../@fury/shared/loading-overlay/loading-overlay.module';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { FurySharedModule } from '../../../@fury/fury-shared.module';

//WIDGETS
import { QuickInfoWidgetModule } from './widgets/quick-info-widget/quick-info-widget.module';


@NgModule({
  imports: [
    CommonModule,
    HomeRoutingModule,
    FurySharedModule,
    MaterialModule,
    FuryCardModule,
    LoadingOverlayModule,
    //WIDGETS
    QuickInfoWidgetModule
  ],
  declarations: [HomeComponent]
})
export class HomeModule {
}
