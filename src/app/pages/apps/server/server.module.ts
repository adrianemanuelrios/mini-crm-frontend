import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../../../../@fury/shared/material-components.module';
import { ListModule } from '../../../../@fury/shared/list/list.module';
import { ServerRoutingModule } from './server-routing.module';
import { ServerComponent } from './server.component';
import { FurySharedModule } from '../../../../@fury/fury-shared.module';

// WIDGETS
import { QuickInfoWidgetModule } from './widgets/quick-info-widget/quick-info-widget.module';

@NgModule({
  imports: [
    CommonModule,
    ServerRoutingModule,
    FurySharedModule,
    MaterialModule,
    ListModule,
    // WIDGETS
    QuickInfoWidgetModule
  ],
  declarations: [ServerComponent]
})
export class ServerModule {
}
