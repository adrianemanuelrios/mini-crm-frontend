import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomersRoutingModule } from './customers-routing.module';
import { CustomersComponent } from './customers.component';
import {FurySharedModule} from '../../../../@fury/fury-shared.module';
import {MaterialModule} from '../../../../@fury/shared/material-components.module';
import {FuryCardModule} from '../../../../@fury/shared/card/card.module';
import {LoadingOverlayModule} from '../../../../@fury/shared/loading-overlay/loading-overlay.module';

// WIDGETS
import { QuickInfoWidgetModule } from './widgets/quick-info-widget/quick-info-widget.module';


@NgModule({
  imports: [
    CommonModule,
    CustomersRoutingModule,
    FurySharedModule,
    MaterialModule,
    FuryCardModule,
    LoadingOverlayModule,
    // WIDGETS
    QuickInfoWidgetModule
  ],
  declarations: [CustomersComponent]
})
export class CustomersModule {
}
