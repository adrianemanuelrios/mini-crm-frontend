import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingsRoutingModule } from './settings-routing.module';
import { SettingsComponent } from './settings.component';
import { FurySharedModule } from '../../../@fury/fury-shared.module';

@NgModule({
  imports: [
    CommonModule,
    SettingsRoutingModule,
    FurySharedModule
  ],
  declarations: [SettingsComponent]
})
export class SettingsModule {
}
