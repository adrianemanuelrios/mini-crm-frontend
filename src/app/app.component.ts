import { DOCUMENT } from '@angular/common';
import { Component, Inject, Renderer2 } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { SidenavService } from './layout/sidenav/sidenav.service';
import { ThemeService } from '../@fury/services/theme.service';
import { ActivatedRoute } from '@angular/router';
import { filter } from 'rxjs/operators';
import { Platform } from '@angular/cdk/platform';
import {SidenavItem} from './layout/sidenav/sidenav-item/sidenav-item.interface';

@Component({
  selector: 'fury-root',
  templateUrl: './app.component.html'
})



export class AppComponent {

  constructor(private sidenavService: SidenavService,
              private iconRegistry: MatIconRegistry,
              private renderer: Renderer2,
              private themeService: ThemeService,
              @Inject(DOCUMENT) private document: Document,
              private platform: Platform,
              private route: ActivatedRoute) {
    this.route.queryParamMap.pipe(
      filter(queryParamMap => queryParamMap.has('style'))
    ).subscribe(queryParamMap => this.themeService.setStyle(queryParamMap.get('style')));

    this.iconRegistry.setDefaultFontSetClass('material-icons');
    this.themeService.theme$.subscribe(theme => {
      if (theme[0]) {
        this.renderer.removeClass(this.document.body, theme[0]);
      }

      this.renderer.addClass(this.document.body, theme[1]);
    });

    if (this.platform.BLINK) {
      this.renderer.addClass(this.document.body, 'is-blink');
    }

    const compenentsExamples = [
      {
        name: 'PAGES EXAMPLE',
        type: 'subheading',
        position: 5
      },
      {
        name: 'Tables',
        routeOrFunction: '/apps/tables',
        icon: 'assignment',
        badge: '22',
        badgeColor: '#2196F3',
        position: 6,
      },
      {
        name: 'Drag & Drop',
        routeOrFunction: '/drag-and-drop',
        icon: 'mouse',
        position: 7
      },
      {
        name: 'Components',
        routeOrFunction: '/components',
        icon: 'layers',
        position: 8
      },
      {
        name: 'Forms',
        icon: 'description',
        position: 9,
        subItems: [
          {
            name: 'Form Elements',
            routeOrFunction: '/forms/form-elements',
            position: 1
          },
          {
            name: 'Form Wizard',
            routeOrFunction: '/forms/form-wizard',
            position: 2
          }
        ]
      },
      {
        name: 'WYSIWYG Editor',
        routeOrFunction: '/editor',
        icon: 'format_shapes',
        position: 10
      },
      {
        name: 'Page Layouts',
        icon: 'view_compact',
        position: 11,
        subItems: [
          {
            name: 'Simple',
            routeOrFunction: '/page-layouts/simple',
            position: 1
          },
          {
            name: 'Simple Tabbed',
            routeOrFunction: '/page-layouts/simple-tabbed',
            position: 2
          },
          {
            name: 'Card',
            routeOrFunction: '/page-layouts/card',
            position: 3
          },
          {
            name: 'Card Tabbed',
            routeOrFunction: '/page-layouts/card-tabbed',
            position: 4
          },
        ]
      },
      {
        name: 'Material Icons',
        routeOrFunction: '/icons',
        icon: 'grade',
        position: 12
      },

      {
        name: 'Maps',
        icon: 'map',
        position: 13,
        subItems: [
          {
            name: 'Google Maps',
            routeOrFunction: '/maps/google-maps',
            position: 1
          }
        ],
        badge: '3',
        badgeColor: '#4CAF50'
      },
      {
        name: 'Blank',
        routeOrFunction: '/blank',
        icon: 'picture_in_picture',
        position: 14
      },
      {
        name: 'Coming Soon',
        routeOrFunction: '/coming-soon',
        icon: 'watch_later',
        position: 15
      },
      {
        name: 'Authentication',
        icon: 'lock',
        position: 16,
        subItems: [
          {
            name: 'Login Page',
            routeOrFunction: '/login',
            position: 1
          },
          {
            name: 'Register Page',
            routeOrFunction: '/register',
            position: 2
          },
          {
            name: 'Forgot Password',
            routeOrFunction: '/forgot-password',
            position: 3
          }
        ]
      },
      {
        name: 'Multi-Level Menu',
        icon: 'menu',
        position: 17,
        subItems: [
          {
            name: 'Level 1',
            subItems: [
              {
                name: 'Level 2',
                subItems: [
                  {
                    name: 'Level 3',
                    subItems: [
                      {
                        name: 'Level 4',
                        subItems: [
                          {
                            name: 'Level 5',
                            routeOrFunction: '/level1/level2/level3/level4/level5'
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      },
      /*
      {
        name: 'Calendar',
        routeOrFunction: '/apps/calendar',
        icon: 'date_range',
        position: 20
      },
    {
      name: 'Inbox',
      routeOrFunction: '/apps/inbox',
      icon: 'inbox',
      position: 25
    },
    {
      name: 'Chat',
      routeOrFunction: '/apps/chat',
      icon: 'chat',
      position: 30,
      badge: '14',
      badgeColor: '#009688'
    },


    */
    ];


    this.sidenavService.addItems([
      {
        name: 'APPS',
        position: 1,
        type: 'subheading',
        customClass: 'first-subheading'
      },
      {
        name: 'Reporte',
        routeOrFunction: '/',
        icon: 'insert_chart',
        position: 2,
        pathMatchExact: true
      },
      {
        name: 'Clientes',
        routeOrFunction: '/customers',
        icon: 'group',
        position: 3,
      },
      {
        name: 'Settings',
        routeOrFunction: '/settings',
        icon: 'settings',
        position: 4,
      },
      ...compenentsExamples

    ]);



  }
}
