import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';

const pageExamples = [
  {
    path: 'apps/tables',
    loadChildren: './pages/tables/all-in-one-table/all-in-one-table.module#AllInOneTableModule',
  },
  {
    path: 'components',
    loadChildren: './pages/components/components.module#ComponentsModule',
  },
  {
    path: 'forms/form-elements',
    loadChildren: './pages/forms/form-elements/form-elements.module#FormElementsModule',
  },
  {
    path: 'forms/form-wizard',
    loadChildren: './pages/forms/form-wizard/form-wizard.module#FormWizardModule',
  },
  {
    path: 'drag-and-drop',
    loadChildren: './pages/drag-and-drop/drag-and-drop.module#DragAndDropModule',
  },
  {
    path: 'editor',
    loadChildren: './pages/editor/editor.module#EditorModule',
  },
  {
    path: 'page-layouts/simple',
    loadChildren: './pages/page-layouts/page-layout-simple/page-layout-simple.module#PageLayoutSimpleModule',
  },
  {
    path: 'page-layouts/simple-tabbed',
    loadChildren: './pages/page-layouts/page-layout-simple-tabbed/page-layout-simple-tabbed.module#PageLayoutSimpleTabbedModule',
  },
  {
    path: 'page-layouts/card',
    loadChildren: './pages/page-layouts/page-layout-card/page-layout-card.module#PageLayoutCardModule',
  },
  {
    path: 'page-layouts/card-tabbed',
    loadChildren: './pages/page-layouts/page-layout-card-tabbed/page-layout-card-tabbed.module#PageLayoutCardTabbedModule',
  },
  {
    path: 'icons',
    loadChildren: './pages/icons/icons.module#IconsModule',
  },
  {
    path: 'maps/google-maps',
    loadChildren: './pages/maps/google-maps/google-maps.module#GoogleMapsModule',
  },
  {
    path: 'blank',
    loadChildren: './pages/blank/blank.module#BlankModule',
  },
  {
    path: 'level1/level2/level3/level4/level5',
    loadChildren: './pages/level5/level5.module#Level5Module',
  },
];

const routes: Routes = [
  {
    path: 'login',
    loadChildren: './pages/authentication/login/login.module#LoginModule',
  },
  {
    path: 'register',
    loadChildren: './pages/authentication/register/register.module#RegisterModule',
  },
  {
    path: 'forgot-password',
    loadChildren: './pages/authentication/forgot-password/forgot-password.module#ForgotPasswordModule',
  },
  {
    path: 'coming-soon',
    loadChildren: './pages/coming-soon/coming-soon.module#ComingSoonModule',
  },
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: '',
        loadChildren: './pages/home/home.module#HomeModule',
        pathMatch: 'full'
      },
      {
        path: 'customers',
        loadChildren: './pages/apps/customers/customers.module#CustomersModule'
      },
      {
        path: 'apps/server/:id',
        loadChildren: './pages/apps/server/server.module#ServerModule',
      },
      {
        path: 'settings',
        loadChildren: './pages/settings/settings.module#SettingsModule',
      },
      ...pageExamples
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled',
    preloadingStrategy: PreloadAllModules,
    scrollPositionRestoration: 'enabled',
    anchorScrolling: 'enabled'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
